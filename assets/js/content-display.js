jQuery(document).ready(function ($) {

    var display = antique_sibling_pages_content_display;

    if ($(window).width() <= 768) {
        if (display['on-small-screen'] === 'closed') {
            $('.antique-sibling-pages .antique-block-arrow').addClass('rotate-arrow');
            $('.antique-sibling-pages .antique-block-content').addClass('hide-content');
        } else {
            $('.antique-sibling-pages .antique-block-arrow').removeClass('rotate-arrow');
            $('.antique-sibling-pages .antique-block-content').removeClass('hide-content');
        }
    }

});