<?php

return array(
    'dependencies' => array(
        'wp-blocks',
        'wp-element',
        'wp-editor',
        'wp-i18n',
        'wp-server-side-render'
    ),
    'version' => 'a35cc1c098b69994c9c6d6dc1416bb90'
);
