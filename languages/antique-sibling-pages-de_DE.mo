��    !      $      ,      ,     -     =  �   S       	   "     ,     :     F     N     Z     _     b  %   p     �     �     �     �     �     �     �     �                     1     6     ?     D     I     N     V     \  �  a          .  �   H     ?  	   Q     [     h     t     |  
   �     �     �  *   �     �     �     �     		     	     $	  *   8	  '   c	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	   Antique Plugins Antique Sibling Pages Apply the colors set within the Antique theme. This option only works if the Antique theme is installed and activated. Upon activation of another theme the colors specified above are used. Background color: Behaviour Block Content Block Title Border: Font color: Hide ID Large screen: Pages and posts containing the block: Reset Save changes Settings Show Sibling Pages Small screen: Your settings have been reset. Your settings have been saved. closed content content, sidebar link location open page post sidebar title type Project-Id-Version: Antique Sibling Pages
Report-Msgid-Bugs-To: 
Last-Translator: 
Language-Team: Simon Garbin
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2023-06-02 10:25+0200
PO-Revision-Date: 2023-06-02 10:25+0200
Language: de_DE
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;esc_html__;esc_html_e;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: antique-sibling-pages.php
X-Poedit-SearchPath-1: package.json
X-Poedit-SearchPath-2: settings.php
X-Poedit-SearchPath-3: shared/menu.php
X-Poedit-SearchPath-4: block/block.js
X-Poedit-SearchPath-5: block/block.json
 Antique Plugins Antique Geschwisterseiten Verwende die Farben, die im Antique-Theme festgelegt wurden. Diese Einstellung kann nur angewandt werden, wenn das Antique-Theme installiert und aktiviert ist. Sobald du ein anderes Theme aktivierst, werden die oben festgelegten Farben verwendet. Hintergrundfarbe: Verhalten Block-Inhalt Block-Titel Rahmen: Schriftfarbe: Verstecken ID Großer Bildschirm: Seiten und Posts, die den Block enthalten: Zurücksetzen Änderungen speichern Einstellungen Anzeigen Geschwisterseiten Kleiner Bildschirm: Deine Einstellungen wurden zurückgesetzt. Deine Einstellungen wurden gespeichert. geschlossen Inhalt Inhalt, Seitenleiste Link Stelle offen Seite Post Seitenleiste Titel Typ 