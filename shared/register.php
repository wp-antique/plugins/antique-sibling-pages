<?php

/*
  ----------------------------------------
  Plugins
  ----------------------------------------
 */

if (!function_exists('antique_plugins_is_active')) {

    function antique_plugins_is_active($plugin) {

        if (in_array(
                        $plugin . '/' . $plugin . '.php',
                        apply_filters(
                                hook_name: 'active_plugins',
                                value: get_option('active_plugins')
                        )
                )) {
            return True;
        } else {
            return False;
        }
    }

}

if (!function_exists('antique_plugins_get_info')) {

    function antique_plugins_get_info() {

        $plugins = array(
            'antique-reading-time' => array(
                'domain' => 'reading-time',
                'slug' => 'reading_time',
                'heading' => esc_html__('Reading time', 'antique-reading-time'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-reading-time'
                ),
                'sidebar_args' => array(
                    'display' => '',
                    'add_subpages' => ''
                ),
            ),
            'antique-section-search' => array(
                'domain' => 'section-search',
                'slug' => 'section_search',
                'heading' => esc_html__('Section search', 'antique-section-search'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-section-search'
                ),
                'sidebar_args' => array(
                    'display' => '',
                ),
            ),
            'antique-info-box' => array(
                'domain' => 'info-box',
                'slug' => 'info_box',
                'heading' => esc_html__('Info box', 'antique-info-box'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-info-box'
                ),
                'sidebar_args' => array(
                    'display' => '',
                ),
            ),
            'antique-toc' => array(
                'domain' => 'toc',
                'slug' => 'toc',
                'heading' => esc_html__('Table of contents', 'antique-toc'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-toc'
                ),
                'sidebar_args' => array(
                    'display' => '',
                ),
            ),
            'antique-sibling-pages' => array(
                'domain' => 'sibling-pages',
                'slug' => 'sibling_pages',
                'heading' => esc_html__('Sibling pages', 'antique-sibling-pages'),
                'is-collapsible' => true,
                'has-sidebar-meta' => true,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-sibling-pages'
                ),
                'sidebar_args' => array(
                    'display' => '',
                ),
            ),
            'antique-link-boxes' => array(
                'domain' => 'link-boxes',
                'slug' => 'link_boxes',
                'heading' => esc_html__('Link boxes', 'antique-link-boxes'),
                'is-collapsible' => false,
                'has-sidebar-meta' => false,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-link-boxes'
                ),
                'sidebar_args' => array(
                    'display' => '',
                ),
            ),
            'antique-subtitles' => array(
                'domain' => 'subtitles',
                'slug' => 'subtitles',
                'heading' => esc_html__('Subtitle', 'antique-subtitles'),
                'is-collapsible' => false,
                'has-sidebar-meta' => false,
                'is-active' => antique_plugins_is_active(
                        plugin: 'antique-subtitles'
                ),
                'sidebar_args' => array(
                    'display' => '',
                ),
            ),
        );

        return $plugins;
    }

}

/*
  ----------------------------------------
  FontAwesome style
  ----------------------------------------
 */

if (!function_exists('antique_plugins_font_awesome')) {

    add_action(
            hook_name: 'init',
            callback: 'antique_plugins_font_awesome'
    );

    function antique_plugins_font_awesome() {

        wp_register_style(
                handle: 'antique-font-awesome',
                src: 'https://use.fontawesome.com/releases/v6.4.0/css/all.css',
                deps: array(),
                ver: false,
                media: 'all'
        );
    }

}

/*
  ----------------------------------------
  Style shared by all Antique blocks
  ----------------------------------------
 */

if (!function_exists('antique_plugins_blocks_style')) {

    add_action(
            hook_name: 'init',
            callback: 'antique_plugins_blocks_style'
    );

    function antique_plugins_blocks_style() {

        $handle = 'antique-plugins-blocks';

        wp_register_style(
                handle: $handle,
                src: plugins_url('/shared/css/blocks.css', dirname(__FILE__)),
                deps: array(),
                ver: false,
                media: 'all'
        );
    }

}

if (!function_exists('antique_plugins_blocks_theme_style')) {

    add_action(
            hook_name: 'init',
            callback: 'antique_plugins_blocks_theme_style'
    );

    function antique_plugins_blocks_theme_style() {

        if (wp_get_theme() == 'Antique') {
            $handle = 'antique-plugins-blocks-theme';

            wp_register_style(
                    handle: $handle,
                    src: plugins_url('/shared/css/theme.css', dirname(__FILE__)),
                    deps: array(),
                    ver: false,
                    media: 'all'
            );
        }
    }

}

/*
  ----------------------------------------
  Script for collapsing blocks
  ----------------------------------------
 */

if (!function_exists('antique_plugins_collapse_script')) {

    add_action(
            hook_name: 'init',
            callback: 'antique_plugins_collapse_script'
    );

    function antique_plugins_collapse_script() {

        $plugins = antique_plugins_get_info();
        $localize = array();

        foreach ($plugins as $name => $info) {
            if (!($info['is-active'] && $info['is-collapsible'])) {
                continue;
            }

            $func = 'get_antique_' . $info['slug'] . '_options';
            if (!function_exists($func)) {
                continue;
            }

            $is_closed = $func()['on_small_screen'] == 'closed';
            $localize[$info['domain']] = $is_closed;
        }

        $handle = 'antique-plugins-collapse';

        wp_register_script(
                handle: $handle,
                src: plugins_url('shared/js/collapse.js', dirname(__FILE__)),
                deps: array('jquery'),
                ver: false,
                in_footer: true
        );

        wp_localize_script(
                handle: $handle,
                object_name: 'antique_plugins_collapse_options',
                l10n: $localize
        );
    }

}

/*
  ----------------------------------------
  Admin style
  ----------------------------------------
 */

if (!function_exists('antique_plugins_admin_style')) {

    add_action(
            hook_name: 'admin_enqueue_scripts',
            callback: 'antique_plugins_admin_style'
    );

    function antique_plugins_admin_style() {

        $handle = 'antique-plugins-admin';

        wp_register_style(
                handle: $handle,
                src: plugins_url('/shared/css/admin.css', dirname(__FILE__)),
                deps: array(),
                ver: false,
                media: 'all'
        );

        wp_enqueue_style(handle: $handle);
    }

}

/*
  ----------------------------------------
  Admin script
  ----------------------------------------
 */

if (!function_exists('antique_plugins_admin_script')) {

    add_action(
            hook_name: 'admin_enqueue_scripts',
            callback: 'antique_plugins_admin_script'
    );

    function antique_plugins_admin_script() {

        $handle = 'antique-plugins-admin';

        wp_register_script(
                handle: $handle,
                src: plugins_url('/shared/js/admin.js', dirname(__FILE__)),
                deps: array('wp-color-picker'),
                ver: false,
                in_footer: true
        );

        wp_enqueue_script(handle: $handle);

        wp_enqueue_style(handle: 'wp-color-picker');
    }

}