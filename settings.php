<?php

function get_antique_sibling_pages_default_options() {

    $defaults = array(
        'heading_font_color' => '#ffffff',
        'heading_bg_color' => '#8e8e8e',
        'heading_border_bool' => '',
        'heading_border_width' => '1',
        'heading_border_style' => 'solid',
        'heading_border_color' => '#000000',
        'content_font_color' => '#000000',
        'content_bg_color' => '#f2f2f2',
        'content_border_bool' => '',
        'content_border_width' => '1',
        'content_border_style' => 'solid',
        'content_border_color' => '#000000',
        'on_large_screen' => 'open',
        'on_small_screen' => 'closed',
        'antique_style' => '',
    );

    return $defaults;
}

function get_antique_sibling_pages_options() {

    $defaults = get_antique_sibling_pages_default_options();

    $options = wp_parse_args(
            args: get_option('antique_sibling_pages_options'),
            defaults: $defaults
    );

    return $options;
}

add_action(
        hook_name: 'admin_menu',
        callback: 'antique_sibling_pages_options_page'
);

function antique_sibling_pages_options_page() {

    if (!function_exists('antique_plugins_admin_menu')) {
        add_menu_page(
                page_title: __('Antique Sibling Pages', 'antique-sibling-pages'),
                menu_title: __('Antique Sibling Pages', 'antique-sibling-pages'),
                capability: 'manage_options',
                menu_slug: 'antique_sibling_pages',
                callback: 'antique_sibling_pages_options_page_html'
        );
    } else {
        add_submenu_page(
                parent_slug: 'antique_plugins',
                page_title: __('Antique Plugins', 'antique-sibling-pages')
                . ' &rsaquo; ' . __('Sibling Pages', 'antique-sibling-pages'),
                menu_title: __('Sibling Pages', 'antique-sibling-pages'),
                capability: 'manage_options',
                menu_slug: 'antique_sibling_pages',
                callback: 'antique_sibling_pages_options_page_html'
        );
    }
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_sibling_pages_remove_menu_page'
);

function antique_sibling_pages_remove_menu_page() {
    if (!function_exists('antique_plugins_admin_menu')) {
        remove_menu_page(menu_slug: 'antique_sibling_pages');
    }
}

function antique_sibling_pages_options_page_html() {

    if (!current_user_can(capability: 'manage_options')) {
        return;
    }
    ?>

    <div class="wrap antique-plugin-settings-wrap">
        <h1><?php echo esc_html(get_admin_page_title()); ?></h1>

        <?php settings_errors(setting: 'antique_sibling_pages_updated_message'); ?>
        <?php settings_errors(setting: 'antique_sibling_pages_reset_message'); ?>

        <form action="options.php" method="post">
            <?php
            settings_fields(option_group: 'antique_sibling_pages_fields');
            do_settings_sections(page: 'antique_sibling_pages');
            ?>

            <div class="antique-plugin-submit-buttons-wrap">
                <?php
                submit_button(
                        text: __('Save changes', 'antique-sibling-pages'),
                        type: 'primary',
                        name: 'updated',
                        wrap: false
                );
                submit_button(
                        text: __('Reset', 'antique-sibling-pages'),
                        type: 'secondary',
                        name: 'reset',
                        wrap: false
                );
                ?>
            </div>

            <?php antique_sibling_pages_where_is_used(); ?>
        </form>
    </div>

    <?php
}

function antique_sibling_pages_settings_sanitize_cb($input) {

    if (isset($_POST['updated'])) {

        add_settings_error(
                setting: 'antique_sibling_pages_updated_message',
                code: 'antique_sibling_pages_updated_message_code',
                message: __('Your settings have been saved.', 'antique-sibling-pages'),
                type: 'updated'
        );
    } else if (isset($_POST['reset'])) {

        add_settings_error(
                setting: 'antique_sibling_pages_reset_message',
                code: 'antique_sibling_pages_reset_message_code',
                message: __('Your settings have been reset.', 'antique-sibling-pages'),
                type: 'updated'
        );

        return array();
    }

    return $input;
}

add_action(
        hook_name: 'admin_init',
        callback: 'antique_sibling_pages_settings_init'
);

function antique_sibling_pages_settings_init() {

    $option_name = 'antique_sibling_pages_options';

    register_setting(
            option_group: 'antique_sibling_pages_fields',
            option_name: $option_name,
            args: array(
                'sanitize_callback' => 'antique_sibling_pages_settings_sanitize_cb'
            )
    );

    add_settings_section(
            id: 'antique_sibling_pages_settings_title_section',
            title: __('Block Title', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_settings_title_section_cb',
            page: 'antique_sibling_pages'
    );

    add_settings_field(
            id: 'heading_font_color',
            title: __('Font color:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_heading_font_color_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_font_color',
            )
    );

    add_settings_field(
            id: 'heading_bg_color',
            title: __('Background color:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_heading_bg_color_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_bg_color',
            )
    );

    add_settings_field(
            id: 'heading_border',
            title: __('Border:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_heading_border_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_title_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'heading_border',
            )
    );

    add_settings_section(
            id: 'antique_sibling_pages_settings_content_section',
            title: __('Block Content', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_settings_content_section_cb',
            page: 'antique_sibling_pages'
    );

    add_settings_field(
            id: 'content_font_color',
            title: __('Font color:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_content_font_color_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_font_color',
            )
    );

    add_settings_field(
            id: 'content_bg_color',
            title: __('Background color:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_content_bg_color_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_bg_color',
            )
    );

    add_settings_field(
            id: 'content_border',
            title: __('Border:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_content_border_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'content_border',
            )
    );

    add_settings_field(
            id: 'antique_style',
            title: '',
            callback: 'antique_sibling_pages_field_antique_style_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_content_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'antique_style',
            )
    );

    add_settings_section(
            id: 'antique_sibling_pages_settings_behaviour_section',
            title: __('Behaviour', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_settings_behaviour_section_cb',
            page: 'antique_sibling_pages'
    );

    add_settings_field(
            id: 'on_large_screen',
            title: __('Large screen:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_on_large_screen_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_behaviour_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'on_large_screen',
            )
    );

    add_settings_field(
            id: 'on_small_screen',
            title: __('Small screen:', 'antique-sibling-pages'),
            callback: 'antique_sibling_pages_field_on_small_screen_cb',
            page: 'antique_sibling_pages',
            section: 'antique_sibling_pages_settings_behaviour_section',
            args: array(
                'option_name' => $option_name,
                'label_for' => 'on_small_screen',
            )
    );
}

function antique_sibling_pages_settings_title_section_cb($args) {
    
}

function antique_sibling_pages_field_heading_font_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_sibling_pages_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#ffffff"
           autocomplete="off"
           >

    <?php
}

function antique_sibling_pages_field_heading_bg_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_sibling_pages_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#8e8e8e"
           autocomplete="off"
           >

    <?php
}

function antique_sibling_pages_field_heading_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_sibling_pages_options();

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <div class="border-customization">

        <input type="checkbox"
               class="enable-border"
               id="<?php echo $str_border_bool; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
               <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
               autocomplete="off"
               >

        <div class="border-settings">
            <input type="number"
                   id="<?php echo $str_border_width; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
                   value="<?php echo esc_attr($options[$str_border_width]); ?>"
                   min="1" max="10"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_border_width; ?>">px</label>

            <select id="<?php echo $str_border_style; ?>"
                    name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                    autocomplete="off"
                    >

                <option value="solid" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'solid', false) ) :
                        ( '' );
                ?>
                        >solid</option>

                <option value="double" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'double', false) ) :
                        ( '' );
                ?>
                        >double</option>
            </select>

            <input type="text"
                   class="color-field"
                   id="<?php echo $str_border_color; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
                   value="<?php echo esc_attr($options[$str_border_color]); ?>"
                   data-default-color="#000000"
                   autocomplete="off"
                   >
        </div>

    </div>

    <?php
}

function antique_sibling_pages_settings_content_section_cb($args) {
    
}

function antique_sibling_pages_field_content_font_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_sibling_pages_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#000000"
           autocomplete="off"
           >

    <?php
}

function antique_sibling_pages_field_content_bg_color_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_sibling_pages_options();
    ?>

    <input type="text"
           class="color-field"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           value="<?php echo esc_attr($options[$field_id]); ?>"
           data-default-color="#f2f2f2"
           autocomplete="off"
           >

    <?php
}

function antique_sibling_pages_field_content_border_cb($args) {

    $option_name = $args['option_name'];
    $options = get_antique_sibling_pages_options();

    $field_id = esc_attr($args['label_for']);
    $str_border_bool = esc_attr($field_id . '_bool');
    $str_border_width = esc_attr($field_id . '_width');
    $str_border_style = esc_attr($field_id . '_style');
    $str_border_color = esc_attr($field_id . '_color');
    ?>

    <div class="border-customization">

        <input type="checkbox"
               class="enable-border"
               id="<?php echo $str_border_bool; ?>"
               name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_bool; ?>]"
               <?php echo $options[$str_border_bool] != '' ? 'checked="checked"' : ''; ?>
               autocomplete="off"
               >

        <div class="border-settings">
            <input type="number"
                   id="<?php echo $str_border_width; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_width; ?>]"
                   value="<?php echo esc_attr($options[$str_border_width]); ?>"
                   min="1" max="10"
                   style="width: 60px"
                   autocomplete="off"
                   >
            <label for="<?php echo $str_border_width; ?>">px</label>

            <select id="<?php echo $str_border_style; ?>"
                    name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_style; ?>]"
                    autocomplete="off"
                    >

                <option value="solid" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'solid', false) ) :
                        ( '' );
                ?>
                        >solid</option>

                <option value="double" <?php
                echo isset($options[$str_border_style]) ?
                        ( selected($options[$str_border_style], 'double', false) ) :
                        ( '' );
                ?>
                        >double</option>
            </select>

            <input type="text"
                   class="color-field"
                   id="<?php echo $str_border_color; ?>"
                   name="<?php echo esc_attr($option_name); ?>[<?php echo $str_border_color; ?>]"
                   value="<?php echo esc_attr($options[$str_border_color]); ?>"
                   data-default-color="#000000"
                   autocomplete="off"
                   >
        </div>

    </div>

    <?php
}

function antique_sibling_pages_field_antique_style_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_sibling_pages_options();
    ?>

    <input type="checkbox"
           id="<?php echo $field_id; ?>"
           name="<?php echo $option_name; ?>[<?php echo $field_id; ?>]"
           <?php echo $options[$field_id] != '' ? 'checked="checked"' : ''; ?>
           autocomplete="off"
           >
    <label for="<?php echo $field_id; ?>"><?php
        esc_html_e('Apply the colors set within the Antique theme. This '
                . 'option only works if the Antique theme is installed and '
                . 'activated. Upon activation of another theme the colors '
                . 'specified above are used.',
                'antique-sibling-pages');
        ?></label>

    <?php
}

function antique_sibling_pages_settings_behaviour_section_cb($args) {
    
}

function antique_sibling_pages_field_on_large_screen_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_sibling_pages_options();
    ?>

    <select id="<?php echo $field_id; ?>"
            name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
            autocomplete="off"
            >

        <option value="open" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'open', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('open', 'antique-sibling-pages'); ?></option>

        <option value="closed" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'closed', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('closed', 'antique-sibling-pages'); ?></option>
    </select>

    <?php
}

function antique_sibling_pages_field_on_small_screen_cb($args) {

    $field_id = esc_attr($args['label_for']);

    $option_name = esc_attr($args['option_name']);
    $options = get_antique_sibling_pages_options();
    ?>

    <select id="<?php echo $field_id; ?>"
            name="<?php echo esc_attr($option_name); ?>[<?php echo $field_id; ?>]"
            autocomplete="off"
            >

        <option value="open" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'open', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('open', 'antique-sibling-pages'); ?></option>

        <option value="closed" <?php
        echo isset($options[$field_id]) ?
                ( selected($options[$field_id], 'closed', false) ) :
                ( '' );
        ?>
                ><?php esc_html_e('closed', 'antique-sibling-pages'); ?></option>
    </select>

    <?php
}

function antique_sibling_pages_where_is_used() {

    $theme_is_antique = wp_get_theme() == 'Antique';
    $search_for = '<!-- wp:antique-sibling-pages/block ';
    $meta_key = '_antique_theme_sibling_pages_meta_key';

    $query = new WP_Query(array(
        'post_type' => array('post', 'page'),
        'posts_per_page' => -1
    ));

    $pages = array();

    if ($query->have_posts()) {
        while ($query->have_posts()) {

            $query->the_post();
            $id = get_the_ID();

            $is_in_content = strpos(
                    haystack: get_the_content($id),
                    needle: $search_for
            );

            if ($theme_is_antique) {

                $meta_exists = metadata_exists(
                        meta_type: 'post',
                        object_id: $id,
                        meta_key: $meta_key
                );

                if ($meta_exists) {
                    $meta = get_post_meta(
                            post_id: get_the_ID(),
                            key: $meta_key,
                            single: true
                    );
                    $meta_is_checked = $meta != '';
                } else {
                    $meta_is_checked = false;
                }
            } else {
                $meta_is_checked = false;
            }

            $post_type_en = get_post_type();
            if ($post_type_en == 'page') {
                $post_type = esc_html__('page', 'antique-sibling-pages');
            } else if ($post_type_en == 'post') {
                $post_type = esc_html__('post', 'antique-sibling-pages');
            } else {
                $post_type = $post_type_en;
            }

            if ($is_in_content && $meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('content, sidebar', 'antique-sibling-pages'),
                    'link' => get_permalink(),
                );
            } else if ($is_in_content && !$meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('content', 'antique-sibling-pages'),
                    'link' => get_permalink()
                );
            } else if (!$is_in_content && $meta_is_checked) {
                $pages[$id] = array(
                    'post_type' => $post_type,
                    'title' => get_the_title(),
                    'loc' => esc_html__('sidebar', 'antique-sibling-pages'),
                    'link' => get_permalink()
                );
            }
        }
    }

    ksort($pages);

    wp_reset_postdata();
    ?>

    <div class="antique-plugin-table-wrap">
        <p><?php
            esc_html_e('Pages and posts containing the block:',
                    'antique-sibling-pages');
            ?></p>
        <div id="antique-plugin-toggle-table">
            <span class="show is-displayed"
                  ><?php esc_html_e('Show', 'antique-sibling-pages'); ?></span>
            <span class="hide"
                  ><?php esc_html_e('Hide', 'antique-sibling-pages'); ?></span>
        </div>

        <table class="antique-plugin-pages-table">
            <tr>
                <th><?php esc_html_e('ID', 'antique-sibling-pages'); ?></th>
                <th><?php esc_html_e('type', 'antique-sibling-pages'); ?></th>
                <th><?php esc_html_e('title', 'antique-sibling-pages'); ?></th>
                <th><?php esc_html_e('location', 'antique-sibling-pages'); ?></th>
                <th><?php esc_html_e('link', 'antique-sibling-pages'); ?></th>
            </tr>
            <?php
            foreach ($pages as $ID => $page) {
                ?>
                <tr>
                    <td><?php esc_html_e($ID); ?></td>
                    <td><?php echo $page['post_type']; ?></td>
                    <td><?php echo $page['title']; ?></td>
                    <td><?php echo $page['loc']; ?></td>
                    <td><a href="<?php echo $page['link']; ?>"
                           target="_blank"><?php
                               esc_html_e('link', 'antique-sibling-pages');
                               ?></a></td>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>

    <?php
}
