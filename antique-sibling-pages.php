<?php
/**
 * Plugin Name: Antique Sibling Pages
 * Plugin URI: https://gitlab.com/wp-antique/plugins/antique-sibling-pages
 * Description:
 * Version: 1.0.0
 * Author: Simon Garbin
 * Author URI: https://simongarbin.com
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html.en
 * Text Domain: antique-sibling-pages
 * Domain Path: /languages
 */
/*
  Copyright 2023 Simon Garbin
  Antique Sibling Pages is free software: you can redistribute it and/or
  modify it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  any later version.

  Antique Sibling Pages is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Antique Sibling Pages. If not, see
  https://www.gnu.org/licenses/gpl-3.0.html.en.
 */
/*
  Comments:
  - Every function is prefixed by the plugin name in order to prevent
  name collisions.
  - In almost all functions named arguments are used in order to facilitate
  the understanding of the code.
  - Single quotation marks are used for php/js code, double quotation marks
  for HTML.
 */

include plugin_dir_path(__FILE__) . '/shared/menu.php';
include plugin_dir_path(__FILE__) . '/shared/register.php';
include plugin_dir_path(__FILE__) . '/settings.php';

defined('ABSPATH') || exit;

/*
  ----------------------------------------
  Translation
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_sibling_pages_load_textdomain'
);

function antique_sibling_pages_load_textdomain() {
    load_plugin_textdomain(
            domain: 'antique-sibling-pages',
            deprecated: false,
            plugin_rel_path: dirname(plugin_basename(__FILE__)) . '/languages'
    );
}

function antique_sibling_pages_translation_dummy() {
    $plugin_description = __(' ',
            'antique-sibling-pages');
}

/*
  ----------------------------------------
  Admin Settings
  ----------------------------------------
 */

add_filter(
        hook_name: 'plugin_action_links_' . plugin_basename(__FILE__),
        callback: 'antique_sibling_pages_settings_link'
);

function antique_sibling_pages_settings_link(array $links) {

    $url = admin_url(path: 'admin.php?page=antique_sibling_pages');
    $link = '<a href="' . esc_html($url) . '">'
            . __('Settings', 'antique-sibling-pages')
            . '</a>';
    $links[] = $link;

    return $links;
}

/*
  ----------------------------------------
  Styles and Scripts
  ----------------------------------------
 */

add_action(
        hook_name: 'wp_head',
        callback: 'antique_sibling_pages_custom_style'
);

add_action(
        hook_name: 'enqueue_block_editor_assets',
        callback: 'antique_sibling_pages_custom_style'
);

function antique_sibling_pages_custom_style() {

    $options = get_antique_sibling_pages_options();

    $heading_has_border = $options['heading_border_bool'] == 'on';
    $content_has_border = $options['content_border_bool'] == 'on';
    $is_antique = $options['antique_style'] == 'on' && wp_get_theme() == 'Antique';

    $heading_bg_color = $is_antique ? 'var(--antique-blocks--heading--bg-color)' :
            $options['heading_bg_color'];
    $heading_font_color = $is_antique ? 'var(--antique-blocks--heading--font-color)' :
            $options['heading_font_color'];
    $heading_border_color = $is_antique ? 'var(--antique-blocks--heading--border-color)' :
            $options['heading_border_color'];
    $heading_border = $heading_has_border ? sprintf(
                    '%spx %s %s',
                    $options['heading_border_width'],
                    $options['heading_border_style'],
                    $heading_border_color
            ) : 'none';

    $content_bg_color = $is_antique ? 'var(--antique-blocks--content--bg-color)' :
            $options['content_bg_color'];
    $content_font_color = $is_antique ? 'var(--antique-blocks--content--font-color)' :
            $options['content_font_color'];
    $content_border_color = $is_antique ? 'var(--antique-blocks--content--border-color)' :
            $options['content_border_color'];
    $content_border = $content_has_border ? sprintf(
                    '%spx %s %s',
                    $options['content_border_width'],
                    $options['content_border_style'],
                    $content_border_color
            ) : 'none';

    $style = sprintf(
            '<style id="antique-sibling-pages-custom-css">'
            . ':root {'
            . '--antique-sibling-pages--heading--bg-color: %s;'
            . '--antique-sibling-pages--heading--font-color: %s;'
            . '--antique-sibling-pages--heading--border: %s;'
            . '--antique-sibling-pages--content--bg-color: %s;'
            . '--antique-sibling-pages--content--font-color: %s;'
            . '--antique-sibling-pages--content--border: %s;'
            . '}'
            . '</style>',
            $heading_bg_color,
            $heading_font_color,
            $heading_border,
            $content_bg_color,
            $content_font_color,
            $content_border
    );

    echo $style;
}

/*
  ----------------------------------------
  Sibling Pages
  ----------------------------------------
 */

add_action(
        hook_name: 'init',
        callback: 'antique_sibling_pages_register_block'
);

function antique_sibling_pages_register_block() {

    $asset_file = include( plugin_dir_path(__FILE__) . '/block/block.asset.php');
    $script_handle = 'antique-sibling-pages-block-script';

    wp_register_script(
            handle: $script_handle,
            src: plugins_url('block/block.js', __FILE__),
            deps: $asset_file['dependencies'],
            ver: $asset_file['version']
    );

    wp_set_script_translations(
            handle: $script_handle,
            domain: 'antique-sibling-pages',
            path: plugin_dir_path(__FILE__) . '/languages/'
    );

    register_block_type(
            block_type: plugin_dir_path(__FILE__) . '/block/',
            args: array(
                'api_version' => 2,
                'render_callback' => 'antique_sibling_pages_render_callback'
            )
    );
}

function antique_sibling_pages_render_callback($block_attributes, $content) {
    return get_the_antique_sibling_pages();
}

function get_the_antique_sibling_pages() {

    $options = get_antique_sibling_pages_options();
    $is_closed = $options['on_large_screen'] == 'closed';

    $parent_page = get_post_parent(post: get_the_ID());
    if (!$parent_page) {
        return;
    }

    $parent_page_title = get_the_title(post: $parent_page->ID);
    $parent_page_url = get_permalink(post: $parent_page->ID);
    $siblings = array_reverse(
            get_children(
                    array(
                        'post_parent' => $parent_page->ID,
                        'post_status' => 'publish',
                        'orderby' => 'title'
                    )
            )
    );

    $sorted_siblings = array();

    foreach ($siblings as $sibling) {
        $sibling_url = get_permalink(post: $sibling->ID);
        $num = antique_sibling_pages_get_num_from_url(url: $sibling_url);
        $sorted_siblings[$num] = $sibling;
    }

    ksort($sorted_siblings);

    $list = '<div class="antique-sibling-pages antique-block">'
            . '<div class="antique-sibling-pages-heading-wrap antique-block-heading-wrap">'
            . '<a href="'
            . $parent_page_url
            . '" class="antique-sibling-pages-heading antique-block-heading">'
            . $parent_page_title
            . '</a>'
            . '<span class="antique-block-arrow antique-block-show-content'
            . ($is_closed ? ' rotate-arrow' : '')
            . '">'
            . '<i class="fa fa-angle-down"></i>'
            . '</span>'
            . '</div>'
            . '<div class="antique-sibling-pages-list-wrap antique-block-content'
            . ($is_closed ? ' hide-content' : '')
            . '">'
            . '<ul>';

    foreach ($sorted_siblings as $sibling) {

        $sibling_title = get_the_title(post: $sibling->ID);
        $sibling_link = get_permalink(post: $sibling->ID);
        $is_selected = get_the_ID() === $sibling->ID;

        $list .= '<li ';
        $list .= $is_selected ? 'class="is-selected"' : '';
        $list .= '>'
                . '<a href="' . $sibling_link . '">'
                . $sibling_title
                . '</a>'
                . '</li>';
    }

    $list .= '</ul>'
            . '</div>'
            . '</div>';

    return $list;
}

function antique_sibling_pages_get_num_from_url($url) {

    $url_hierarchy = explode('/', $url);
    $url_from_page = $url_hierarchy[array_key_last($url_hierarchy)];
    $url_from_page_sep = explode('-', $url_from_page);
    $num = $url_from_page_sep[array_key_last($url_from_page_sep)];

    return $num;
}
